package utilities;

public class MatrixMethod {
	//creates a duplicate array where each entry is repeated (Ex: {{1,2},{2,3}} -> {{1,1,2,2},{2,2,3,3}} )
	public static int[][] duplicate(int[][] original){
		int [][] duplicate = new int[original.length][original[0].length * 2];
		
		for(int i = 0 ; i<original.length;i++) {
			int dupIndex = 0;
			for (int j = 0 ; j < original[i].length;j++) {
				duplicate[i][dupIndex] = original[i][j];
				dupIndex++;
				duplicate[i][dupIndex] = original[i][j];
				dupIndex++;
			}
		}
		
		return duplicate;
	}
	
}
