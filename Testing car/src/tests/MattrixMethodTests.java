package tests;
import utilities.MatrixMethod;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MattrixMethodTests {
	// test the duplicate method in MatrixMethod.java
	@Test
	public void testDuplicate() {
		int[][] original= { {1,3,4} , {2,67,900} };
		int[][] expectedResult= { {1,1,3,3,4,4} , {2,2,67,67,900,900} };
		assertArrayEquals(expectedResult,MatrixMethod.duplicate(original) );
	}

}
