package tests;

import automobiles.Car;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {
	//testing  the constructor and get methods of Car.java
	@Test
	public void testConstructorAndGetMethods() {
		Car c = new Car(10);
		assertEquals(10,c.getSpeed());
		assertEquals(50,c.getLocation());
	}
	//testing the moveRight method of Car.java
	@Test
	public void testMoveRight() {
		Car c1 = new Car(10);
		c1.moveRight();
		assertEquals(60,c1.getLocation());
		Car c2 = new Car(60);
		c2.moveRight();
		assertEquals(100,c2.getLocation());
	}

	//testing the moveLeft method of Car.java
	@Test
	public void testMoveLeft() {
		Car c1 = new Car(10);
		c1.moveLeft();
		assertEquals(40,c1.getLocation());
		Car c2 = new Car(60);
		c2.moveLeft();
		assertEquals(0,c2.getLocation());
	}

	//testing the accelerate method of Car.java
	@Test
	public void testAccelerate() {
		Car c  = new Car(10);
		c.accelerate();
		assertEquals(11,c.getSpeed());
	}

	//testing the stop method of Car.java
	@Test
	public void testStop() {
		Car c  = new Car(10);
		c.stop();
		assertEquals(0,c.getSpeed());
	}
}
